<?php
/*
Plugin Name: Prueba Técnica
Plugin URI: 
Description: Plugin para la prueba técnica
Version: 1.0
Author: Daniel Durán Martín
Author URI: 
License: 
*/

//Taxonomía
function crearTaxonomiaJerarquica(){
    $etiquetas = array(
        'name' => __('Especie'),
        'singular_name' => __('Especie'),
        'search_items' =>  __('Buscar especies'),
        'all_items' => __('Todos las especies'),
        'parent_item' => __('Especie padre'),
        'parent_item_colon' => __('Especie padre:'),
        'edit_item' => __('Editar especie'), 
        'update_item' => __('Actualizar especie'),
        'add_new_item' => __('Agregar una nueva especie'),
        'menu_name' => __('Especies'),
    );

    register_taxonomy(
        'especies',
        array('product'),
        array(
            'hierarchical' => true, 
            'labels' => $etiquetas,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'especie'),
        )
    );
}

add_action('init', 'crearTaxonomiaJerarquica', 0);

//Array de las especies que hay en el Wordpress
function listaCategorias(){
	$resultado = "";
    $arrayVueltas = array();

	$argumentos = array(
		'taxonomy'	=> "especies",
		'parent'	=> 0,
		'hide_empty' => 0
	);

	$categorias = get_categories($argumentos);

	foreach($categorias as $categoria){
        $resultado .= $categoria->cat_name." ";

        array_push($arrayVueltas, $resultado);
    }

    $array = explode(" ", $resultado, count($arrayVueltas));

	return $array;
}

function listaSubcategorias($parent){
	
}

//Shortcode
function shortcodeEspecies(){
    $array = listaCategorias();
    $options = "";

    foreach($array as $elemento){
        $elemento = str_replace(" ", "", $elemento);
        $options .= '<option value="http://localhost/wordpress/especie/'.strtolower($elemento).'/">'.$elemento.'</option>';
    }

    $resultado = '
    <form name="link"><select name="menu">
        <option selected="selected">Elige un enlace</option>'.$options.'
    </select><input onclick="location=document.link.menu.options[document.link.menu.selectedIndex].value;" type="button" value="Ir"/></form>';

    return $resultado;
}

add_shortcode('especies', 'shortcodeEspecies');

//Productos relacionados
function productosRelacionados($productosRelacionados, $idProducto) {
    $terms = wp_get_post_terms($idProducto, 'especies');

    $array = [];
	
    foreach ($terms as $term) {
        $array[] = $term -> term_id;
    }
	
    $productosRelacionados = get_posts(array(
        'post_type' => 'product',
		'posts_per_page' => -1,
		'post__not_in' => array($idProducto),
		'tax_query' => array( 
	    	array(
			'taxonomy' => 'especies',
			'field' => 'id',
			'terms' => $array
	        )
    	)
    ));

    return $productosRelacionados;
}

add_filter('woocommerce_related_products', 'productosRelacionados', 9999, 3);